#cloud-config
write_files:
  - path: "/etc/hstack/hstack.conf"
    permissions: "0644"
    owner: "root"
    content: | 
      PRIVATE_NETWORK="${ private_network }"
      CONSUL_MODE="server"
      CONSUL_AUTOJOIN="aws"
      CONSUL_AUTOJOIN_AWS_TAG_VALUE="${ autojoin_tag_value }"
      CONSUL_AUTOJOIN_AWS_TAG_KEY="${ autojoin_tag_key }"
      CONSUL_AGENT_TAGS="consul-server,${autojoin_tag_value}"
      CONSUL_BOOTSTRAP_EXPECT=${ server_count }
      CONSUL_OPTS="-ui"
      NTP_MODE="server"
      DOMAIN=${ domain }
      DATACENTER=${ dc }
      CONSUL_ENCRYPT_KEY=${ encrypt_key }
      JOIN_IPV4_ADDR="${ join_ipv4_addr }"
      JOIN_IPV4_ADDR_WAN="${ join_ipv4_addr_wan }"
  - path: "/etc/hstack/certs/rootca.pem"
    permissions: "0644"
    owner: "root"
    encoding: base64
    content: ${root_ca}

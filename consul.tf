#--------------------------------------------------------------
# This module creates all resources necessary for Consul
#--------------------------------------------------------------

data "atlas_artifact" "consul_ami" {
  name = "${var.atlas_username}/aws-${var.region}-coreos-hstack"

  type = "amazon.image"
  metadata {
     version = "${var.hstack_version}"
  }
}

data "aws_subnet" "selected" {
  id = "${element(split(",",var.subnet_ids), 0)}"
}

data "aws_vpc" "selected" {
  id = "${data.aws_subnet.selected.vpc_id}"
}

resource "aws_security_group" "consul_servers" {
  count =  "${var.count> 0 ? 1 : 0}"

  name        = "${var.name}"
  vpc_id      = "${data.aws_vpc.selected.id}"
  description = "Security group for Consul"

  tags      { Name = "${var.name}" }
  lifecycle { create_before_destroy = true }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["${data.aws_vpc.selected.cidr_block}"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Vault is the source of TLS certificates but vault will use consul
# as its storage backend.
#
# Chicken||Egg syndrom!
#
# Consul will be bootstrapped before vault & without SSL enabled
# Then, if vault is enabled & as soon as it gets online, each consul
# server will get its own private cert from vault and reload its configuration

# Consul nodes will also be used as primary ntp servers
# This reflects the ntp deployment architecture described in this blogpost :
# https://blog.logentries.com/2014/03/synchronizing-clocks-in-a-cassandra-cluster-pt-2-solutions/
data "template_file" "consul_user_data" {
  template = "${file("${path.module}/consul.tpl")}"

  vars {
    server_count          = "${var.count}"
    join_ipv4_addr        = "${var.join_ipv4_addr}"
    join_ipv4_addr_wan    = "${var.join_ipv4_addr_wan}"
    private_network       = "${data.aws_vpc.selected.cidr_block}"
    bootstrap_expect      = "${var.count}"
    autojoin_tag_key      = "${var.autojoin_tag_key}"
    autojoin_tag_value    = "${coalesce(var.autojoin_tag_value, var.name)}"
    root_ca               = "${base64encode(var.root_ca)}"
    domain                = "${var.domain}"
    dc                    = "${var.dc}"
    encrypt_key           = "${var.encrypt_key}"
  }
}

resource "aws_launch_configuration" "consul_servers" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix = "${var.name}"
  iam_instance_profile = "${var.iam_instance_profile}"
  image_id    = "${lookup(data.atlas_artifact.consul_ami.metadata_full, format("region-%s", var.region))}"
  instance_type = "${var.instance_type}"
  security_groups = ["${aws_security_group.consul_servers.id}"]

  associate_public_ip_address = "false"
  key_name                = "${var.keypair_name}"

  user_data     = "${data.template_file.consul_user_data.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

//  Auto-scaling group for our cluster.
resource "aws_autoscaling_group" "consul_servers" {
  count =  "${var.count> 0 ? 1 : 0}"

  name_prefix = "${var.name}"
  launch_configuration = "${aws_launch_configuration.consul_servers.name}"
  desired_capacity     = "${var.count}"
  min_size             = "${var.count}"
  max_size             = "${var.count}"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0

  vpc_zone_identifier = [ "${split(",",var.subnet_ids)}"]

  enabled_metrics           = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  tag {
    key                 = "${var.autojoin_tag_key}"
    value               = "${coalesce(var.autojoin_tag_value, var.name)}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

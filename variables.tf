variable "name"                 { default = "consul" }
variable "hstack_version"       { default = "1.0"}
variable "region"               { default = "us-east-1" }

variable "domain"               { default = "consul" }
variable "dc"                   { default = "dc1" }

variable "root_ca"              { default = "" }

variable "subnet_ids"           { }
variable "keypair_name"         { }
variable "atlas_username"       { default = "hstack" }
variable "count"                { default = "-1" }
variable "instance_type"        { default = "t2.medium" }
variable "iam_instance_profile"  { default = "consul" }

variable "encrypt_key"          { }
variable "join_ipv4_addr"            { default = "" }
variable "join_ipv4_addr_wan"        { default = "" }

variable "autojoin_tag_value"   { default = "" }
variable "autojoin_tag_key"     { default = "consul_autojoin" }
